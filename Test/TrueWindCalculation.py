import math


def deg2rad(degrees):
    return float(degrees) * (math.pi / float(180))


def rad2deg(radians):
    return float(radians) * (float(180) / math.pi)


def trueWind(speed, heading, awd, awa, aws):

    if heading and awa and awd:
        print 'awa is overriding awd and heading. twd output value will be invalid.'

    speed = float(speed)
    heading = float(heading)
    awd = float(awd)
    aws = float(aws)

    if awa is not None:
        awa = float(awa)
        if heading:
            awd = heading + awa

    elif heading is not None and awd is not None:
        if awd < heading:
            awd += 360

        awa = awd - heading

    awa = deg2rad(awa)
    aws = aws / speed
    tanAlfa = math.sin(awa) / (aws - math.sin(awa))
    alpha = math.atan(tanAlfa)
    tdiff = rad2deg(alpha + awa)
    tspeed = math.sin(awa) / math.sin(alpha)

    if heading is not None:
        twd = round(tdiff + heading, 2);

        if twd > 360:
            twd -= 360;

    tanAlpha = round(tanAlfa, 5),
    alpha = round(alpha, 5),
    twa = round(tdiff, 2),
    twsr = round(tspeed, 2),
    tws = round(tspeed * speed, 2)

    print tanAlpha
    print alpha
    print twa
    print twsr
    print tws

if __name__ == "__main__":

    trueWind(2.1, 86, 0, None, 3.2)