

class WindData(object):

    def __init__(self):
        self._twa = 0
        self._twd = 0
        self._tws = 0
        self._twsr = 0
        self._aws = 0
        self._awa = 0

    def get_true_wind_angle_to_bow(self):
        return self._twa

    def set_true_wind_angle_to_bow(self, twa):
        self._twa = twa

    def get_true_wind_direction_to_fixed_north(self):
        return self._twd

    def set_true_wind_direction_to_fixed_north(self, twd):
        self._twd = twd

    def get_true_wind_speed(self):
        return self._tws

    def set_true_wind_speed(self, speed):
        self._tws = speed

    def get_true_wind_speed_rounded(self):
        return self._twsr

    def set_true_wind_speed_rounded(self, speed):
        self._twsr = speed

    def get_apparent_wind_speed(self):
        return self._aws

    def set_apparent_wind_speed(self, speed):
        self._aws = speed

    def get_apparent_wind_angle(self):
        return self._awa

    def set_apparent_wind_angle(self, angle):
        self._awa = angle