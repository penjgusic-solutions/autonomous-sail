import sys
from WindWidget import WindWidget
from BoatWidget import BoatWidget
from PyQt4.QtGui import *


if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = QWidget()

    formLayout = QFormLayout()

    verticalBox = QVBoxLayout()
    horizontalBox = QHBoxLayout()
    horizontalBox1 = QHBoxLayout()
    horizontalBox2 = QHBoxLayout()

    wind = WindWidget()
    windBox = QSpinBox()
    windSpeedBox = QSpinBox()

    windLabel = QLabel()
    windLabel.setText("Wind Direction")

    windBox.setRange(0, 359)
    windBox.valueChanged.connect(wind.setAngle)

    formLayout.addRow(windLabel)

    windApparentLabel = QLabel()
    windApparentLabel.setText("Apparent Angle")

    horizontalBox.addWidget(windApparentLabel)
    horizontalBox.addWidget(windBox)

    windApparentLabelSpeed = QLabel()
    windApparentLabelSpeed.setText("Wind Speed")

    horizontalBox1.addWidget(windApparentLabelSpeed)
    horizontalBox1.addWidget(windSpeedBox)

    verticalBox.addLayout(horizontalBox)
    verticalBox.addLayout(horizontalBox1)

    horizontalBox2.addLayout(verticalBox)
    horizontalBox2.addWidget(wind)

    formLayout.addRow(horizontalBox2)

    # Boat Widget
    boat_verticalBox = QVBoxLayout()
    boat_horizontalBox = QHBoxLayout()
    boat_horizontalBox1 = QHBoxLayout()
    boat_horizontalBox2 = QHBoxLayout()
    boat_verticalBox = QVBoxLayout()

    boat = BoatWidget()
    boatBox = QSpinBox()
    boatSpeedBox = QSpinBox()
    boatLabel = QLabel("Heading")
    boatBox.setRange(0, 359)
    boatBox.valueChanged.connect(boat.setAngle)
    boatSpeedBox.valueChanged.connect(boat.setBoatSpeed)
    windSpeedBox.valueChanged.connect(boat.setWindSpeed)
    windBox.valueChanged.connect(boat.setWindAngle)

    windSpeedBox.setValue(2)

    formLayout.addRow(boatLabel)

    boat_heading = QLabel("Boat heading")

    boat_horizontalBox.addWidget(boat_heading)
    boat_horizontalBox.addWidget(boatBox)

    boatSpeedLabel = QLabel("Boat Speed")

    boat_horizontalBox1.addWidget(boatSpeedLabel)
    boat_horizontalBox1.addWidget(boatSpeedBox)

    boat_verticalBox.addLayout(boat_horizontalBox)
    boat_verticalBox.addLayout(boat_horizontalBox1)

    boat_horizontalBox2.addLayout(boat_verticalBox)
    boat_horizontalBox2.addWidget(boat)

    formLayout.addRow(boat_horizontalBox2)

    # true wind
    trueWind_verticalBox = QVBoxLayout()
    trueWind_horizontalBox = QHBoxLayout()
    trueWind_horizontalBox1 = QHBoxLayout()
    trueWind_horizontalBox2 = QHBoxLayout()
    trueWind_verticalBox = QVBoxLayout()

    trueWind = WindWidget()
    trueWindTextBlock = QDoubleSpinBox()
    trueWindTextBlock.setMaximum(360.00)

    trueWindTextBlock.setEnabled(False)
    boat.trueWindChanged.connect(trueWindTextBlock.setValue)

    boat.trueWindChanged.connect(trueWind.setAngle)

    trueWindLabel = QLabel('True wind direction')

    formLayout.addRow(trueWindLabel)

    trueWindLabel = QLabel("True wind angle")

    trueWind_horizontalBox.addWidget(trueWindLabel)
    trueWind_horizontalBox.addWidget(trueWindTextBlock)

    trueWindSpeedLabel = QLabel("Nmea 0183 message")

    trueWind_horizontalBox1.addWidget(trueWindSpeedLabel)

    trueWind_verticalBox.addLayout(trueWind_horizontalBox)
    trueWind_verticalBox.addLayout(trueWind_horizontalBox1)

    trueWind_horizontalBox2.addLayout(trueWind_verticalBox)
    trueWind_horizontalBox2.addWidget(trueWind)

    trueWindSpeedLineEdit = QLineEdit()
    trueWindSpeedLineEdit.setEnabled(False)
    boat.messageChanged.connect(trueWindSpeedLineEdit.setText)

    formLayout.addRow(trueWind_horizontalBox2)
    formLayout.addRow(trueWindSpeedLineEdit)

    window.setLayout(formLayout)

    window.setFixedSize(800,600)

    window.show()
    sys.exit(app.exec_())



