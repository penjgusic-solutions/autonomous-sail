import math
from Model.WindData import WindData


def deg2rad(degrees):
    return float(degrees) * (math.pi / float(180))


def rad2deg(radians):
    return float(radians) * (float(180) / math.pi)


class TrueWindCalculator(object):

    def __init__(self, speed, heading, awd, awa, aws):

        self._boat_speed = speed
        self._heading = heading
        self._apparent_wind_direction = awd
        self._apparent_wind_angle = awa
        self._apparent_wind_speed = aws

    def calculate_true_wind(self):

        try:
            if self._heading and self._apparent_wind_angle and self._apparent_wind_direction:
                print 'awa is overriding awd and heading. twd output value will be invalid.'

            if self._boat_speed <= 0.0:
                wind_data = WindData()
                wind_data.set_true_wind_angle_to_bow(self._apparent_wind_direction)
                wind_data.set_true_wind_speed(self._apparent_wind_speed)
                return wind_data

            speed = float(self._boat_speed)
            heading = float(self._heading)
            awd = float(self._apparent_wind_direction)
            aws = float(self._apparent_wind_speed)

            if self._apparent_wind_angle is not None:
                awa = float(self._apparent_wind_angle)
                if heading:
                    awd = heading + awa

            elif heading is not None and awd is not None:
                if awd < heading:
                    awd += 360

                awa = awd - heading

            awa = deg2rad(awa)
            aws = aws / speed
            tanAlfa = math.sin(awa) / (aws - math.sin(awa))
            alpha = math.atan(tanAlfa)
            tdiff = rad2deg(alpha + awa)
            tspeed = math.sin(awa) / math.sin(alpha)

            if heading is not None:
                twd = round(tdiff + heading, 2)

                if twd > 360:
                    twd -= 360;

            tanAlpha = round(tanAlfa, 5),
            alpha = round(alpha, 5),
            twa = round(tdiff, 2),
            twsr = round(tspeed, 2),
            tws = round(tspeed * speed, 2)

            wind_data = WindData()

            wind_data.set_true_wind_angle_to_bow(twa[0])
            wind_data.set_true_wind_speed(tws)
            wind_data.set_true_wind_direction_to_fixed_north(twd)

            return wind_data

        except Exception as e:
            print str(e)
