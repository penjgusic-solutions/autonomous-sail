import math
import pynmea2
from Calculator.TrueWindCalculator import TrueWindCalculator


class SailManager(object):
    def __init__(self, message, heading, boat_speed):
        self._mwv_message = message
        self._heading = heading
        self._boat_speed = boat_speed
        self._boat_mode_angle = 80
        self._no_sail_angle = 45

    def optimal_position(self):
        message = ''
        try:
            message = pynmea2.parse(self._mwv_message)

        except:
            print "Unable to parse sentence"

        wind_angle = message.wind_angle

        try:
            calculator = TrueWindCalculator(self._boat_speed, self._heading, wind_angle, None, message.wind_speed)

            data = calculator.calculate_true_wind()

            angle_to_the_bow = None
            true_wind_direction = None

            if data is not None:
                angle_to_the_bow = data.get_true_wind_angle_to_bow()
                true_wind_direction = data.get_true_wind_direction_to_fixed_north()

            sail_angle = self.calculate_sail_position(angle_to_the_bow,true_wind_direction)

            return sail_angle

        except Exception as e:
            print str(e)

    def calculate_sail_position(self, angle_to_the_bow, true_wind_direction):

        if angle_to_the_bow is None:
            tup = (180 + self._heading, False, true_wind_direction)
            return tup

        if angle_to_the_bow < self._no_sail_angle or angle_to_the_bow > 360.0 - self._no_sail_angle:
            sail_angle = self._heading + 180
            tup = (sail_angle, False, true_wind_direction)
            return tup

        elif 0 < angle_to_the_bow <= 90:
            sail_angle = angle_to_the_bow - self._no_sail_angle + 180 + self._heading
            tup = (sail_angle, True, true_wind_direction)
            return tup

        elif 90 < angle_to_the_bow <= 180:
            sail_angle = angle_to_the_bow / 2 + 180 + self._heading
            tup = (sail_angle, True, true_wind_direction)
            return tup

        elif 180 < angle_to_the_bow <= 270:
            sail_angle = angle_to_the_bow / 2 + self._heading
            tup = (sail_angle, True, true_wind_direction)
            return tup

        elif 270 < angle_to_the_bow <= 360:
            sail_angle = angle_to_the_bow - 270 + 135 + self._heading

            tup = (sail_angle, True, true_wind_direction)
            return tup

        else:
            tup = (180 + self._heading, False, true_wind_direction)
            return tup
