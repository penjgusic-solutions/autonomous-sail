from PyQt4.QtCore import *
from PyQt4.QtGui import *
import pynmea2
from Manager.SailManager import SailManager


class BoatWidget(QWidget):

    angleChanged = pyqtSignal(float)
    sailAngleChanged = pyqtSignal(float)
    windSpeedChanged = pyqtSignal(float)
    windAngleChanged = pyqtSignal(float)
    boatSpeedChanged = pyqtSignal(float)
    trueWindChanged = pyqtSignal(float)
    messageChanged = pyqtSignal(str)

    def __init__(self, parent=None):

        QWidget.__init__(self, parent)

        self._angle = 0.0
        self._can_sail = False
        self._sailAngle = 180.0
        self._windAngle = 359.0
        self._windSpeed = 2.10
        self._boatSpeed = 1.0
        self._true_wind_angle = 0.0
        self._mvvmMessage = ''

        self._margins = 10
        self._pointText = {0: "N", 45: "NE", 90: "E", 135: "SE", 180: "S", 225: "SW", 270: "W", 315: "NW"}
        self.resize(200,200)

    def paintEvent(self, event):

        painter = QPainter()
        painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)

        painter.fillRect(event.rect(), self.palette().brush(QPalette.Window))
        self.drawMarkings(painter)
        self.drawNeedle(painter)
        self.drawSail(painter)

        painter.end()

    def generate_data(self):
        sentence = pynmea2.MWV('II', 'MWV', (str(self._windAngle), 'R', str(self._windSpeed), 'M'))
        self._mvvmMessage = str(sentence)
        manager = SailManager(str(sentence), self._angle, self._boatSpeed)
        optimal_position = manager.optimal_position()
        if optimal_position is not None and optimal_position.count >= 3:
            self._sailAngle = optimal_position[0]
            self._can_sail = optimal_position[1]
            self._true_wind_angle = optimal_position[2]

    def drawMarkings(self, painter):

        painter.save()
        painter.translate(self.width() / 2, self.height() / 2)
        scale = min((self.width() - self._margins) / 120.0, (self.height() - self._margins) / 120.0)
        painter.scale(scale, scale)

        font = QFont(self.font())
        font.setPixelSize(10)
        metrics = QFontMetricsF(font)

        painter.setFont(font)
        painter.setPen(self.palette().color(QPalette.Shadow))

        i = 0
        while i < 360:

            if i % 45 == 0:
                painter.drawLine(0, -40, 0, -50)
                painter.drawText(-metrics.width(self._pointText[i]) / 2.0, -52,
                                 self._pointText[i])
            else:
                painter.drawLine(0, -45, 0, -50)

            painter.rotate(15)
            i += 15

        painter.restore()

    def drawNeedle(self, painter):

        painter.save()
        painter.translate(self.width() / 2, self.height() / 2)
        painter.rotate(self._angle)
        scale = min((self.width() - self._margins) / 120.0, (self.height() - self._margins) / 120.0)
        painter.scale(scale, scale)

        painter.setPen(QPen(Qt.NoPen))
        painter.setBrush(self.palette().brush(QPalette.Shadow))

        painter.drawPolygon(
            QPolygon([QPoint(-15, 0), QPoint(-10, -20), QPoint(-5, -30), QPoint(-2, -35), QPoint(0, -40), QPoint(2,-35), QPoint(5, -30), QPoint(10, -20), QPoint(15, 0), QPoint(15, 45),
                      QPoint(-15, 45), QPoint(-15, 0)])
        )

        painter.setBrush(self.palette().brush(QPalette.Highlight))

        painter.restore()

    def drawCubicPath(self, can_sail):

        start_point = QPointF(0, 0)
        end_point = QPointF(0, -45)

        if not can_sail:
            control_point1 = QPointF(-10, -20)
            control_point2 = QPointF(10, -20)
        else:
            control_point1 = QPointF(-10, -20)
            control_point2 = QPointF(-7, -20)

        cubic_path = QPainterPath(start_point)
        cubic_path.cubicTo(control_point1, control_point2, end_point)

        return cubic_path

    def drawSail(self, painter):

        painter.save()
        painter.translate(self.width() / 2, self.height() / 2)
        painter.rotate(self._sailAngle)
        scale = min((self.width() - self._margins) / 120.0, (self.height() - self._margins) / 120.0)
        painter.scale(scale, scale)

        painter.setPen(QPen(Qt.NoPen))
        painter.setBrush(self.palette().brush(QPalette.Light))

        cubic_path = self.drawCubicPath(self._can_sail)
        painter.drawPath(cubic_path)

        painter.setBrush(self.palette().brush(QPalette.Highlight))

        painter.restore()

    def sizeHint(self):
        return QSize(150, 150)

    def angle(self):
        return self._angle

    def sailAngle(self):
        return self._sailAngle

    def windSpeed(self):
        return self._windSpeed

    def windAngle(self):
        return self._windAngle

    def boatSpeed(self):
        return self._boatSpeed

    def trueWind(self):
        return self._true_wind_angle

    def message(self):
        return self._mvvmMessage

    @pyqtSlot(str)
    def setMessage(self, message):
        self._mvvmMessage = message
        self.messageChanged.emit(message)
        self.update()

    @pyqtSlot(float)
    def setTrueWindAngle(self, true_wind):

        if self._true_wind_angle != true_wind:
            self._true_wind_angle = true_wind
            self.generate_data()
            self.trueWindChanged.emit(true_wind)
            self.messageChanged.emit(self._mvvmMessage)
            self.update()

    @pyqtSlot(float)
    def setBoatSpeed(self, boatSpeed):

        if boatSpeed != self._boatSpeed:
            if boatSpeed <= 0.0:
                self._boatSpeed = 1.0
            self._boatSpeed = boatSpeed
            self.generate_data()
            self.boatSpeedChanged.emit(boatSpeed)
            self.messageChanged.emit(self._mvvmMessage)
            self.update()

    @pyqtSlot(float)
    def setWindAngle(self, windAngle):
        if self._windAngle != windAngle:
            self._windAngle = windAngle
            self.generate_data()
            self.windAngleChanged.emit(windAngle)
            self.trueWindChanged.emit(self._true_wind_angle)
            self.messageChanged.emit(self._mvvmMessage)
            self.update()

    @pyqtSlot(float)
    def setWindSpeed(self, windSpeed):

        if self._windSpeed != windSpeed:
            self._windSpeed = windSpeed
            self.generate_data()
            self.windSpeedChanged.emit(windSpeed)
            self.trueWindChanged.emit(self._true_wind_angle)
            self.messageChanged.emit(self._mvvmMessage)
            self.update()

    @pyqtSlot(float)
    def setSailAngle(self, angle):

        if angle != self._sailAngle:
            self._sailAngle = angle
            self.sailAngleChanged.emit(angle)
            self.trueWindChanged.emit(self._true_wind_angle)
            self.messageChanged.emit(self._mvvmMessage)
            self.update()

    @pyqtSlot(float)
    def setAngle(self, angle):

        if angle != self._angle:
            self._angle = angle
            self.generate_data()
            self.angleChanged.emit(angle)
            self.trueWindChanged.emit(self._true_wind_angle)
            self.messageChanged.emit(self._mvvmMessage)
            self.update()

    angle = pyqtProperty(float, angle, setAngle)
    sailAngle = pyqtProperty(float, sailAngle, setSailAngle)
    windSpeed = pyqtProperty(float,windSpeed, setWindSpeed)
    windAngle = pyqtProperty(float, windAngle, setWindAngle)
    boatSpeed = pyqtProperty(float, boatSpeed, setBoatSpeed)
    trueWindAngle = pyqtProperty(float, trueWind, setTrueWindAngle)
    message = pyqtProperty(str, message, setMessage)



